$(document).ready(function() {
    let spacer = $('.navbar').outerHeight();
    var date = new Date();
    $('.spacer').css({'height' : spacer});
    $('#date').text(date.getFullYear());
    $('#pagepiling').pagepiling({
        menu: null,
        direction: 'vertical',
        verticalCentered: true,
        sectionsColor: [],
        anchors: [],
        scrollingSpeed: 700,
        easing: 'swing',
        loopBottom: false,
        loopTop: false,
        css3: true,
        navigation: {
            'textColor': '#000',
            'bulletsColor': '#fff',
            'position': 'right',
            'tooltips': ['Home', 'Equipments', 'Property Development', 'Logistics', 'Food', 'Consumer Goods', 'Medical', 'Laboratory', 'About Us']
        },
        normalScrollElements: null,
        normalScrollElementTouchThreshold: 0,
        touchSensitivity: 0,
        keyboardScrolling: true,
        sectionSelector: '.section',
        animateAnchor: false,
        afterLoad: function(anchorLink, index){},
        afterRender: function(){},
        onLeave: function(index, nextIndex, direction){ }
    });
});



$(document).ready(function(){
    $('.nav-logo').on('click', function(){
        window.location.href = "index.html";
    })
})


$(window).on('load',function() {
    $(".loader").fadeOut('slow');
});